
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hotelbookingios/client/hive_names.dart';
import 'package:hotelbookingios/src/app/app.dart';
import 'package:hotelbookingios/src/models/hive_db_models/age_model.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:hive_flutter/hive_flutter.dart';
// import 'package:android_play_install_referrer/android_play_install_referrer.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocDir = await path_provider.getApplicationDocumentsDirectory();
  await Firebase.initializeApp();
  // await CountryCodes.init();
  // await Hive.initFlutter();
  // Hive.registerAdapter(AgeAdapter());
  // await Hive.openBox<Age>(HiveBoxes.age);
  Hive.init(appDocDir.path);
  await Hive.openBox('adult');
  await Hive.openBox('code');
  await Hive.openBox('locale');
  await Hive.openBox('region');
  await Hive.openBox('date');
  await Hive.openBox('age');
  await Hive.openBox('room');
  await Hive.openBox('referrerAPI');
  // await Hive.initFlutter();
  // Hive.registerAdapter(AgeAdapter());
  // await Hive.openBox<Age>(HiveBoxes.age);
  runApp(MyApp());
}

// class App extends StatelessWidget {
//   // Create the initialization Future outside of `build`:
//   final Future<FirebaseApp> _initialization = Firebase.initializeApp();

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: FutureBuilder(
//         // Initialize FlutterFire:
//         future: _initialization,
//         builder: (context, snapshot) {
//           // Check for errors
//           if (snapshot.hasError) {
//             return MyApp();
//           }

//           // Once complete, show your application
//           if (snapshot.connectionState == ConnectionState.done) {
//             return MyApp();
//           }

//           // Otherwise, show something whilst waiting for initialization to complete
//           return Scaffold(
//             body: Center(
//               child: Text('connecting to firebase...'),
//             ),
//           );
//         },
//       ),
//     );

//   }
// }
