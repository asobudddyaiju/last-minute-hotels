import 'package:hotelbookingios/src/models/state.dart';
import 'package:hotelbookingios/src/resources/api_providers/user_api_provider.dart';

/// Repository is an intermediary class between network and data
class Repository {
  final userApiProvider = UserApiProvider();

  Future<State> sampleCall() => userApiProvider.sampleCall();

  Future<State> destinationList({String searchKey}) =>
      userApiProvider.destinationList(searchKey: searchKey);
}
