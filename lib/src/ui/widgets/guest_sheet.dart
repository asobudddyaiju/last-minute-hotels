import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hotelbookingios/src/ui/widgets/childrenCountUI.dart';
import 'package:hotelbookingios/src/ui/widgets/room_person_count.dart';
import 'package:hotelbookingios/src/utils/utils.dart';

class GuestBottomSheet extends StatefulWidget {
  @override
  _GuestBottomSheetState createState() => _GuestBottomSheetState();
}
int numberOfRooms = 1;
int num = 0;
List<int> adults = [0,0,0,0,0,0,0,0,0,0,];
List<int> child = [0,0,0,0,0,0,0,0,0,0,];
List<int> childCount=[0,0,0];
String ageVal = '';

class _GuestBottomSheetState extends State<GuestBottomSheet> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: screenHeight(context,dividedBy: 10),
                width: screenWidth(context,dividedBy: 1),
                child: RoomPersonCount(
                  counterLabel: "Room",
                  counterValue: numberOfRooms,
                  incFunction:(){
                    setState(() {
                      numberOfRooms++;
                    });
                  },
                  decFunction: (){
                    setState(() {
                      numberOfRooms--;
                    });
                  },
                ),
              ),
            ),
            ListView.builder(
              shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                padding: EdgeInsets.only(
                    left: screenWidth(context, dividedBy: 100),
                    right: screenWidth(context, dividedBy: 100)),
                itemCount: numberOfRooms,
                itemBuilder: (BuildContext context, int index){
              return SingleChildScrollView(
                child: Column(
                  children: [
                    RoomPersonCount(
                      counterLabel: "Adult",
                      counterValue: adults[index],
                      incFunction: (){

                        setState(() {
                          adults[index]++;
                        });
                      },
                      decFunction: (){
                        setState(() {
                          adults[index]--;
                        });
                      },
                    ),
                    RoomPersonCount(
                      counterLabel: "Children",
                      counterValue: child[index],
                      incFunction: (){
                        setState(() {
                          if(child[index]<3) {
                            ++child[index];
                            ++childCount[index];
                          }
                        });
                      },
                      decFunction: (){
                        setState(() {
                          if(child[index]>0) {
                            child[index]--;
                            childCount[index]--;
                          }
                        });
                      },
                    ),
                    ListView.builder(
                      shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: childCount[index],
                        padding: EdgeInsets.only(
                            left: screenWidth(context, dividedBy: 100),
                            right: screenWidth(context, dividedBy: 100)),
                        itemBuilder:(BuildContext context, int childIndex){
                      return ChildrenCountUI(
                        childrenCount: childIndex+1,
                        hintText: "",
                        getage:(val){
                          ageVal=val;
                        },
                      );
                    } )
                  ],
                ),
              );
            })
          ],
        ),
      ),
    );
  }
}
