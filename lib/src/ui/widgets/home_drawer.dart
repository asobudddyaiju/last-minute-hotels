// import 'package:country_codes/country_codes.dart';
import 'dart:io';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import 'package:hotelbookingios/localization/locale.dart';
import 'package:hotelbookingios/src/auth_service/firebase_auth.dart';
import 'package:hotelbookingios/src/ui/screens/currency.dart';
import 'package:hotelbookingios/src/ui/screens/destination_page.dart';
import 'package:hotelbookingios/src/ui/screens/login.dart';
import 'package:hotelbookingios/src/ui/screens/profile_page.dart';
import 'package:hotelbookingios/src/ui/screens/region.dart';
import 'package:hotelbookingios/src/utils/constants.dart';
import 'package:hotelbookingios/src/utils/object_factory.dart';
import 'package:hotelbookingios/src/utils/utils.dart';

class HomeDrawer extends StatefulWidget {
  final FirebaseAuth auth, authfb;

  const HomeDrawer({this.auth, this.authfb});

  @override
  // _HomeDrawerState createState() => _HomeDrawerState();
  State<StatefulWidget> createState() {
    return _HomeDrawerState();
  }
}

class _HomeDrawerState extends State<HomeDrawer> {
  String buttonName = "Sign_in";
  String photpurl;
  String dummyphotourl =
      'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/Antu_system-switch-user.svg/768px-Antu_system-switch-user.svg.png';
  String displayName;
  String dummydisplayName = 'username';
// CountryDetails details = CountryCodes.detailsForLocale();
  String defaultLang = Platform.localeName;



  @override
  void initState() {
    FirebaseAnalytics().logEvent(name: 'drawer_page',parameters: null);
    if ((widget.auth.currentUser != null) ||
        (widget.authfb.currentUser != null)) {
      setState(() {
        buttonName = "Sign_Out";
        photpurl = Hive.box('lang').get(12);
        displayName = Hive.box('lang').get(13);
      });
    } else {
      setState(() {
        photpurl = dummyphotourl;
        displayName = dummydisplayName;
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: ListView(
        children: <Widget>[
          SizedBox(
            height: screenHeight(context, dividedBy: 20),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 15.0),
            child: Row(
              children: [
                Container(
                  width: 38.0,
                  height: 40.24,
                  decoration: BoxDecoration(
                    color: Colors.black.withOpacity(.05),
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: NetworkImage(photpurl), fit: BoxFit.fill),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15.0),
                  child: Text(displayName == dummydisplayName ? getTranslated(context, displayName):displayName,
                      style: TextStyle(
                          color: Color(0xFF272727),
                          fontWeight: FontWeight.w300,
                          fontSize: 18.0,
                          fontStyle: FontStyle.normal,
                          fontFamily: 'NexaLight')),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 40),
            child: MaterialButton(
              color: Constants.kitGradients[0],
              onPressed: () {
                if ((widget.auth.currentUser == null) &&
                    (widget.authfb.currentUser == null)) {
                  FirebaseAnalytics().logEvent(name: 'clicked_signin_button',parameters: null);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              Login(auth: widget.auth, authFb: widget.authfb)));
                } else {
                  FirebaseAnalytics().logEvent(name: 'clicked_signout_button',parameters: null);
                  void signout() async {
                    await Auth(auth: widget.auth, authfb: widget.authfb)
                        .Signout();
                  }

                  signout();

                  setState(() {
                    buttonName = "Sign_in";
                    photpurl = dummyphotourl;
                    displayName = dummydisplayName;
                  });
                }
              },
              minWidth: 10,
              height: 40,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)),
              child: Text(
                getTranslated(context, buttonName),
                style: TextStyle(fontSize: 18, color: Colors.white),
              ),
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 30),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: 1,
            color: Colors.grey.withOpacity(.10),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 15),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: GestureDetector(
              onTap: () {
                FirebaseAnalytics().logEvent(name: 'clicked_destination_button',parameters: null);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DestinationPage()));
              },
              child: ListTile(
                leading: SvgPicture.asset("assets/images/search_icon.svg"),
                title: Text(
                  getTranslated(context, 'Search'),
                  style: TextStyle(
                      color: Colors.black.withOpacity(.50),
                      fontWeight: FontWeight.w200),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: GestureDetector(
              onTap: () {
                FirebaseAnalytics().logEvent(name: 'clicked_profile_button',parameters: null);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProfilePage(
                              auth: widget.auth,
                              authfb: widget.authfb,
                            )));
              },
              child: ListTile(
                leading: Icon(
                  Icons.settings,
                  color: Colors.black.withOpacity(.60),
                ),
                title: Text(
                  getTranslated(context, 'Profile'),
                  style: TextStyle(
                      color: Colors.black.withOpacity(.60),
                      fontWeight: FontWeight.w200),
                ),
              ),
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 4.7),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: 1,
            color: Colors.grey.withOpacity(.10),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap:  () {
                  FirebaseAnalytics().logEvent(name: 'clicked_region_button',parameters: null);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => RegionPage()));
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: screenHeight(context, dividedBy: 100),
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: Text(
                    getTranslated(context, 'language'),
                    style: TextStyle(fontSize: 14, color: Colors.black),
                  ),
                ),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(
                      // vertical: screenHeight(context, dividedBy: 100),
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: GestureDetector(
                      onTap: () {
                        FirebaseAnalytics().logEvent(name: 'clicked_region_button',parameters: null);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => RegionPage()));
                      },
                      child: Text(
                        Hive.box('lang').get(2) != null
                            ? Hive.box('lang').get(2)
                            :ObjectFactory().getLanguage.getCountryLanguage(defaultLang)[1]
                        ,
                        // details.name,
                        style: TextStyle(fontSize: 12),
                      )))
            ],
          ),
          SizedBox(height: screenHeight(context,dividedBy: 75),),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  FirebaseAnalytics().logEvent(name: 'clicked_currency_button',parameters: null);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CurrencyPage()));
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: screenHeight(context, dividedBy: 100),
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: Text(
                    getTranslated(context, 'Currency'),
                    style: TextStyle(fontSize: 14, color: Colors.black),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    // vertical: screenHeight(context, dividedBy: 100),
                    horizontal: screenWidth(context, dividedBy: 20)),
                child: GestureDetector(
                    onTap: () {
                      FirebaseAnalytics().logEvent(name: 'clicked_currency_button',parameters: null);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CurrencyPage()));
                    },
                    child: Text(
                        Hive.box('code').get(2) != null
                            ? Hive.box('code').get(2).toString().split(" ").first
                            :Hive.box('room').get('country')!=null?ObjectFactory().getCurrency.getCurrency(Hive.box('room').get('currency')):"USD",
                        overflow: TextOverflow.visible,
                        style: TextStyle(fontSize: 12))),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
