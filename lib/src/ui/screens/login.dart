import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:hotelbookingios/localization/locale.dart';
import 'package:hotelbookingios/src/auth_service/firebase_auth.dart';
import 'package:hotelbookingios/src/ui/screens/home_page.dart';
import 'package:hotelbookingios/src/utils/constants.dart';
import 'package:hotelbookingios/src/utils/object_factory.dart';
import 'package:hotelbookingios/src/utils/utils.dart';


class Login extends StatefulWidget {
  final FirebaseAuth auth, authFb;

  const Login({this.auth, this.authFb});

  @override
  State<StatefulWidget> createState() {
    return _LoginState();

  }
}

class _LoginState extends State<Login> {
  bool _isLoading;
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    FirebaseAnalytics().logEvent(name: 'login_page',parameters: null);
    _isLoading = false;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 15)),
          child: AppBar(
            leading: new IconButton(
              onPressed: () {
                FirebaseAnalytics().logEvent(name: 'clicked_back_button',parameters: null);
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                        (route) => false);
              },
              icon: new Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
              ),
            ),
            backgroundColor: Constants.kitGradients[13],
            elevation: 0.5,
          )),
        key: _globalKey,
        body: Container(
          width: screenWidth(context,dividedBy: 1),
          height: screenHeight(context,dividedBy: 1),
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Color(0xFF15548F),Colors.white,Colors.white,Colors.white, Colors.white],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter),
          ),
          child: SafeArea(
          child: Center(
            child: Stack(
              children: [
                Column(
                  children: [
                    Center(
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * 0.4,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [ Constants.kitGradients[13], Colors.white],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter),
                        ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(vertical: screenHeight(context,dividedBy: 6.0),horizontal: screenWidth(context,dividedBy: 2.5 )),
                          child:SvgPicture.asset(
                            "assets/images/app_icon_final.svg",
                            fit: BoxFit.fill,color: Colors.white,
                          ),),
                      ),
                    ),
                    Center(
                      child: Text(
                        getTranslated(context, 'Sign_In_and_Save'),
                        style: TextStyle(
                            fontFamily: 'NexaBold',
                            fontWeight: FontWeight.w700,
                            color: Color(0xFF484848),
                            fontSize: 18.0,
                            fontStyle: FontStyle.normal),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: 10.0, bottom: 10.0, left: 20.0, right: 20.0),
                      child: Center(
                        child: Text(
                          getTranslated(context, 'Track_prices,_organize_travel_plans_and_access_member-only_with_your_HotelBooking_account'),
                          style: TextStyle(
                              fontFamily: 'NexaLight',
                              fontWeight: FontWeight.w300,
                              color: Color(0xFF7E7E7E),
                              fontSize: 10.0,
                              fontStyle: FontStyle.normal),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                      EdgeInsets.only(left: 35.0, right: 35.0, top: 20.0),
                      child: Center(
                        child: GestureDetector(
                          onTap: () async {
                            FirebaseAnalytics().logEvent(name: 'clicked_google_signin_button',parameters: null);
                            googleSignin();
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 60.0,
                            decoration: BoxDecoration(
                                border: Border.all(style: BorderStyle.none),
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(30.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xFFDBE7EB).withOpacity(0.2),
                                      offset: Offset(0, 5),
                                      blurRadius: 1.0,
                                      spreadRadius: 1.0)
                                ]),
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset('assets/images/google_icon.png',
                                      width: 20.0, height: 20.0),
                                  Padding(
                                    padding: EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      getTranslated(context, 'Continue_with_Google'),
                                      style: TextStyle(
                                          fontFamily: 'NexaLight',
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w300,
                                          fontStyle: FontStyle.normal,
                                          color: Color(0xFF484848)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                      EdgeInsets.only(left: 35.0, right: 35.0, top: 20.0),
                      child: Center(
                        child: GestureDetector(
                          onTap: () async {
                            FirebaseAnalytics().logEvent(name: 'clicked_facebook_signin_button',parameters: null);
                            facebookSignin();
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 60.0,
                            decoration: BoxDecoration(
                                border: Border.all(style: BorderStyle.none),
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(30.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xFFDBE7EB).withOpacity(0.2),
                                      offset: Offset(0, 5),
                                      spreadRadius: 1.0,
                                      blurRadius: 1.0)
                                ]),
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset('assets/images/facebook_icon.png', width: 20.0, height: 20.0),
                                  Padding(
                                    padding: EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      getTranslated(context, 'Continue_with_Facebook'),
                                      style: TextStyle(
                                          fontFamily: 'NexaLight',
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w300,
                                          fontStyle: FontStyle.normal,
                                          color: Color(0xFF484848)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SafeArea(
                      child: Center(
                        child: Padding(
                          padding: EdgeInsets.only(
                              top: 50.0, bottom: 0.0, left: 15.0, right: 15.0),
                          child: RichText(
                            text: TextSpan(
                                text:getTranslated(context, 'By_signing_up_you_agree_to_our'),
                                style: TextStyle(
                                  fontFamily: 'NexaLight',
                                  fontWeight: FontWeight.w300,
                                  color: Color(0xFF404040),
                                  fontSize: 10.0,
                                ),
                                children: <TextSpan>[
                                  TextSpan(
                                      recognizer: TapGestureRecognizer()..onTap = () => print('you just clicked terms and conditions'),
                                      text: getTranslated(context, 'Terms_and_conditions'),
                                      style: TextStyle(
                                          fontFamily: 'NexaLight',
                                          fontWeight: FontWeight.w300,
                                          color: Colors.blue,
                                          fontSize: 10.0)),
                                  TextSpan(
                                      text: ' '+ getTranslated(context,'And'),
                                      style: TextStyle(
                                          fontFamily: 'NexaLight',
                                          fontWeight: FontWeight.w300,
                                          fontSize: 10.0,
                                          color: Color(0xFF404040))),
                                  TextSpan(
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () => print(
                                            'you just clicked Privacy Policy'),
                                      text: ' '+getTranslated(context, 'Privacy_policy'),
                                      style: TextStyle(
                                          fontFamily: 'NexaLight',
                                          fontWeight: FontWeight.w300,
                                          color: Colors.blue,
                                          fontSize: 10.0))
                                ]),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                _isLoading == true
                    ? Center(
                  child: Container(
                      height: 40.0,
                      width: 40.0,
                      child: Center(child: CircularProgressIndicator())),
                )
                    : Stack()
              ],
            ),
          ),
        ),),
      );

  }
  void _showSnackbar(String snackdata) {
    final snack = SnackBar(
      content: Text(snackdata,style: TextStyle(fontFamily: "Poppins",fontStyle: FontStyle.normal,fontSize: 12.0),),
      duration: Duration(seconds: 2),
      backgroundColor: Constants.kitGradients[0],
    );
    _globalKey.currentState.showSnackBar(snack);
  }
  void googleSignin()async{
    setState(() {
      _isLoading = true;
    });
    String googlesigninstatus =
    await Auth(
        auth: widget.auth,
        authfb: widget.authFb)
        .signInWithGoogle();
    if (googlesigninstatus == "Success") {
      FirebaseAnalytics().logEvent(name: 'google_signin_success',parameters: null);
      Navigator.pushAndRemoveUntil( context,
          MaterialPageRoute(
              builder: (context) => HomePage()), (route) => false);

    } else {
      FirebaseAnalytics().logEvent(name: 'google_signin_error',parameters: null);
      setState(() {
        _isLoading=false;
      });
      _showSnackbar(googlesigninstatus.trim());
    }
  }
  void facebookSignin()async{
    setState(() {
      _isLoading = true;
    });
    String status = await Auth(
        auth: widget.auth, authfb: widget.authFb)
        .signInWithFacebook();
    if (status == "Success") {
      FirebaseAnalytics().logEvent(name: 'facebook_signin_success',parameters: null);
      Navigator.pushAndRemoveUntil(  context,
          MaterialPageRoute(
              builder: (context) => HomePage()), (route) => false);
    } else {
      if(
      status.trim()=='An account already exists with the same email address but different sign-in credentials. Sign in using a provider associated with this email address.')
      {
        FirebaseAnalytics().logEvent(name: 'linking_facebbok_google_auth',parameters: null);
       LinkGoogleAuthAndFacebookAuth();
      }else{
        FirebaseAnalytics().logEvent(name: 'facebook_login_error',parameters: null);
        _showSnackbar(status.trim());
        print(status);
        setState(() {
          _isLoading=false;
        });
      }
    }
  }
  void LinkGoogleAuthAndFacebookAuth()async{
    setState(() {
      _isLoading = true;
    });
    String status = await Auth(
        auth: widget.auth, authfb: widget.authFb)
        .linkGoogleAndFacebook();
    if (status == "Success") {
      FirebaseAnalytics().logEvent(name: 'linked_facebook_google_success',parameters: null);
      Navigator.pushAndRemoveUntil(  context,
          MaterialPageRoute(
              builder: (context) => HomePage()), (route) => false);
    } else {
      _showSnackbar(status.trim());
      print(status);
      setState(() {
        _isLoading=false;
      });
    }
  }
}
