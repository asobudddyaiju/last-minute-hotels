import 'dart:io';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hotelbookingios/localization/locale.dart';
import 'package:hotelbookingios/src/auth_service/firebase_auth.dart';
import 'package:hotelbookingios/src/ui/screens/currency.dart';
import 'package:hotelbookingios/src/ui/screens/home_page.dart';
import 'package:hotelbookingios/src/ui/screens/region.dart';
import 'package:hotelbookingios/src/ui/screens/about_us.dart';
import 'package:hotelbookingios/src/ui/widgets/build_dropcurrency.dart';
import 'package:hotelbookingios/src/ui/widgets/build_dropdown.dart';
import 'package:hotelbookingios/src/ui/widgets/profile_list_tile.dart';
import 'package:hotelbookingios/src/utils/object_factory.dart';
import 'package:hotelbookingios/src/utils/utils.dart';

import 'login.dart';

class ProfilePage extends StatefulWidget {
  FirebaseAuth auth;
  FirebaseAuth authfb;
  ProfilePage({this.auth, this.authfb});
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  String buttonName="Sign_in";
  String photpurl;
  String defaultLang = Platform.localeName;
  String dummyphotourl =
      'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/Antu_system-switch-user.svg/768px-Antu_system-switch-user.svg.png';
  String displayName;
  String dummydisplayName = 'Username';
  @override
  void initState() {
    FirebaseAnalytics().logEvent(name: 'profile_page',parameters: null);
    if ((widget.auth.currentUser != null) ||
        (widget.authfb.currentUser != null)) {
      setState(() {
        buttonName = "Sign_Out";
        photpurl = Hive.box('lang').get(12);
        displayName = Hive.box('lang').get(13);
      });
    } else {
      setState(() {
        photpurl = dummyphotourl;
        displayName = dummydisplayName;
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 15)),
          // here the desired height
          child: AppBar(
            leading: new IconButton(
              icon: new Icon(
                Icons.close,
                color: Colors.black,
              ),
              onPressed: () {
                FirebaseAnalytics().logEvent(name: 'clicked_back_button',parameters: null);
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                    (route) => false);
              },
            ),
            title: Text(
              getTranslated(context, 'Profile'),
              style: TextStyle(
                  color: Colors.black54, fontWeight: FontWeight.normal),
            ),
            elevation: 0,
            backgroundColor: Colors.white,
          )),
      body: ListView(
        children: <Widget>[
          SizedBox(
            height: screenHeight(context, dividedBy: 15),
          ),
          Card(
            elevation: 0.5,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          left: screenWidth(context, dividedBy: 15),
                          right: screenWidth(context, dividedBy: 15),
                          top: screenHeight(context, dividedBy: 75)),
                      child: Text(
                        getTranslated(context, 'language'),
                        style: TextStyle(fontSize: 14, color: Colors.black),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: screenHeight(context, dividedBy: 75),
                          right: screenWidth(context, dividedBy: 50),
                          left: screenWidth(context, dividedBy: 50)),
                      child: GestureDetector(
                        onTap: () {
                          FirebaseAnalytics().logEvent(name: 'clicked_region_button',parameters: null);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => RegionPage()));
                        },
                        child: Text(
                          Hive.box('lang').get(2) != null
                              ? Hive.box('lang').get(2)
                              : ObjectFactory().getLanguage.getCountryLanguage(defaultLang)[1],
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          left: screenWidth(context, dividedBy: 15),
                          right:screenWidth(context, dividedBy: 15),
                          top: screenHeight(context, dividedBy: 100)),
                      child: Text(
                        getTranslated(context, 'Currency'),
                        style: TextStyle(fontSize: 14, color: Colors.black),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(
                            top: screenHeight(context, dividedBy: 75),
                            right: screenWidth(context, dividedBy: 50),
                            left: screenWidth(context, dividedBy: 50),
                            bottom: screenHeight(context, dividedBy: 75)),
                        child: GestureDetector(
                          onTap: () {
                            FirebaseAnalytics().logEvent(name: 'clicked_currency_button',parameters: null);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CurrencyPage()));
                          },
                          child: Text(
                            Hive.box('code').get(2) != null
                                ? Hive.box('code').get(2).toString().split(" ").first
                                : Hive.box('room').get('country')!=null?ObjectFactory().getCurrency.getCurrency(Hive.box('room').get('currency')):"USD",
                            overflow: TextOverflow.visible,
                            style: TextStyle(fontSize: 14),
                          ),
                        )),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: screenHeight(context, dividedBy: 15)),
          Card(
            elevation: 0.5,
            child: Column(children: [
              GestureDetector(
                onTap: () {
                  FirebaseAnalytics().logEvent(name: 'clicked_aboutus_button',parameters: null);
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => AboutPage()));
                },
                child: ProfileListTile(
                  listText: getTranslated(context, 'About_Us'),
                  listText1: '',
                ),
              ),
              ProfileListTile(
                listText: getTranslated(context, 'Help_and_feedback'),
                listText1: '',
              ),
              ProfileListTile(
                listText: getTranslated(context, 'Terms_and_conditions'),
                listText1: '',
              ),
              ProfileListTile(
                listText: getTranslated(context, 'Privacy_policy'),
                listText1: '',
              ),
              GestureDetector(
                onTap: () {
                  if ((widget.auth.currentUser == null) &&
                      (widget.authfb.currentUser == null)) {
                    // Navigator.pushAndRemoveUntil(
                    //  context,
                    //  MaterialPageRoute(
                    //      builder: (context) => Login(auth: widget.auth)),
                    //  (route) => false);
                    FirebaseAnalytics().logEvent(name: 'clicked_signin_button',parameters: null);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Login(
                                auth: widget.auth, authFb: widget.authfb)));
                  } else {
                    FirebaseAnalytics().logEvent(name: 'clicked_signout_button',parameters: null);
                    void signout() async {
                      await Auth(auth: widget.auth, authfb: widget.authfb)
                          .Signout();
                    }

                    signout();

                    setState(() {
                      buttonName = getTranslated(context, 'Sign_in');
                      photpurl = dummyphotourl;
                      displayName = dummydisplayName;
                    });
                  }
                },
                child: ProfileListTile(
                  listText: getTranslated(context, buttonName),
                  listText1: '',
                ),
              ),
            ]),
          )
        ],
      ),
    );
  }
}
