import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class UrlLauncher extends StatefulWidget {
  @override
  _UrlLauncherState createState() => _UrlLauncherState();
}

class _UrlLauncherState extends State<UrlLauncher> {
  final url = 'http://book.hotelmost.com/SearchTermTypeRedirection.ashx?';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('url launcher'),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: _launchURL,
          child: Text('url launcher'),
        ),
      ),
    );
  }

  _launchURL() async {
    if (await canLaunch(url)) {
      await launch(
        url,
        //forceWebView:true,
        enableJavaScript: true,
        enableDomStorage: true,
      );
    } else {
      print('Unable to open URL $url');
    }
  }
}